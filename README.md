# Learning D3.js - The basics

## SVG
SVG allows for simple manipulation. it can scale image with great animation & interaction. 
D3 provdes many helper function that help us represent simple shapes like circle, rectangle.
Can als write text with more ease, no need to use tspan element. 

<g> element is like div, its purpose is to group element disting in the svg. It has no
graphical element. Its a logical grouping element usually use extensively when we create
graphical object that are made up of several elements. 

For Moving around we can use the translate attribute. We also have a <Path>, which has a
d attribute that determine how it should be drawn, it can be either open or close which
is determine by the presence of Z.

D3 Classed() allows us to add and remove class from an element. 

Arrow function does not have the context this. Just like react it must be bind to the 
main class. 


## Chapter 3 : Layouts
/**
 * D3 provides d3.axisLeft(), d3.axisRight(), d3.axisBotton() and d3.axisTop()
 * selection's cakk() method from a selection on g element we want these graphical
 * ekements to be drawn.
 * 
 * it a component that is commpose of a <g.tick> that contains a line. <path.domain> with 
 * equal size to the extent of the axis and finally a label which contans a text
 * for each tick. axisLeft() or axisTop(), are either drawn outside the canvas like
 * earlier rectangle. to move the axes aroung then we must use .attr("translate") of
 * their parent <g> elements, either when we draw them or later. This is why its important
 * to assign ID to our element when we append them to hte canvas. We can move the 
 * x-axis to the bottom.
 *  */


// d3.selectAll("#xAxisG").attr("transform", "translate(0,500)");




/**
 * Now Lets Build A Reasonable Boplot, we'll need a set, plotting the number of registered
 * visitor coming to the website by dat of the weeks so that we 
 */

// d3.select("svg").selectAll("*").remove();



/**
 * D3 Provides an enormous library of example of charts and there are many implementations.
 * It has layouts that allow you to create complex data visualization from a 
 * properly formatted dataset. Three main types of function found in D3 can be 
 * classified as generators, components and layouts.
 * 
 * Generators
 * ex: area(), line(), arc()  
 * they take datapoint (Array Values) and produces the d in path element.
 * it consits of function that take data and return the necessary SVG drawing
 * code to create a graphical object based on the data. it simplify the process of 
 * creating a complex svg path.
 * 
 * For instance if we wave an array of point and we want to draw one line from 
 * one point to another or turn it into a polygon or an area, a few d3 function can
 * get us there.
 * 
 * Components
 * ex: axis(), brush(), zoom()
 * they take in function and return element and event listeners.
 * like an axis is a function for drawing all the graphical elements necessary for
 * an axis. 
 * 
 * In Contrast with generator, which produces the d attribute string for the path.
 * Component create an entire set of graphical objects necessary for a particular
 * chart component. The most commonly use d3 are axis, which create a bunch of 
 * line, path, g and text elements that are needed for an axis based on the scale
 * and settings you provide the function. Another component is d3.bruch which creates
 * all the graphical elements necessary for a brush selector.
 * 
 * Layouts (Takes Data Input and computes visual variables such as position and size.)
 * ex: stack(), pie(), chord()
 * they take in the whole datasets and return newly annotated dataset
 * with attributes for graphical layout of datapoint. Basically add additonal
 * attributes to your dataset so it can be ready 
 * 
 * In Contrast to generator and components, D3 layout are straightforward like the 
 * pie or complex like force directed netweork layout. they take in one or more arrays
 * of data and sometimes generator and apppend attributes to the data necessary
 * to draw it in certain positions or sizes, either statically or dynamically. 
 *  
 * All Charts consist of several graphic element that are drawn or derived from
 * dataset being represebted. These graphical elements may be graphic primitives
 * like circles or rectangle or more complex, multipart, graphical elements like boxplots. 
 * with an understanding of how generators and components, we can recreate
 * beautiful and exotic charts.
 * 
 * Using Circle or Rectangle wont work for all dataset, for instance if we want to 
 * demonstrate an import aspect of the data such as user demographics or stats data.
 * The distribution of the data sometimes get lost.
 */

/**
 * It is good to apply g elements to charts since they allow us to apply labels
 * or other important information to graphical representations. Meaning that need
 * to use the transform attribute which is how g elements are posisioned on the canvas.a1
 */

```js
// Loading external data
d3.csv('/data/sample.csv', (error, dataset) => {
  dataset.forEach((data) => {
    console.log(data)
  })
})
```

## Useful resources
 - [MIT Vis](https://observablehq.com/@mitvis/introduction-to-d3)
 - [Scott Murray D3 Tutorials](http://alignedleft.com/tutorials/d3/)
 - [25+ Resources to Learn D3.js from Scratch](https://blog.modeanalytics.com/learn-d3/)
