import * as d3 from 'd3';

/**
 * Band Scare are convenient for charts
 * with an ordinal or categorical data
 * Scale can only be applied to the element of the domain
 */

let four = d3.scaleBand()
    .domain([1, 2, 3, 4])
    .range([0, 100])

const step = four.step() // 25
const four50 = four.copy().paddingInner(0.5)
const four50Step = four50.step() // should be equal to 2/(3+4) * width



console.log(four(1))
console.log(four(2))
console.log(four(3))
console.log(four(4))

// https://observablehq.com/@d3/d3-scaleband