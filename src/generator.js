import * as d3 from 'd3'

// Learning About D3 Generator.a1
export const learningAboutGenerator = () => {

    // Line Generator do thing like create a string M (Move TO) And L (Line to) command to mive array of points
    const lineGenerator = d3.line() //.defined(d => d !== null);
    const points = [
        [0, 80],
        [100, 100],
        [200, 30],
        [300, 50],
        [400, 40],
        [500, 80]
    ]

    const lineData = [
        { value: 10 },
        { value: 50 },
        { value: 30 },
        { value: 40 },
        { value: 20 },
        { value: 70 },
        { value: 50 }
    ];

    const pointMissing = [
        [0, 80],
        [100, 80],
        null, [300, 50],
        [400, 40],
        [500, 80]
    ];


    const pathData = lineGenerator(points)
    lineGenerator.x((d, i) => console.log("Index: " + i + ". Data: " + d))


    d3.select("body")
        .append("svg")
        .attr("class", "generator")
        .append("path")
        .attr("d", pathData)


    // Radial Line
    const radialLineGen = d3.radialLine().angle(d => d[0]).radius(d => d[1]);
    const radPts = [
        [0, 80],
        [Math.PI * 0.25, 80],
        [Math.PI * 0.5, 30],
        [Math.PI * 0.75, 80],
        [Math.PI, 80],
        [Math.PI * 1.25, 80],
        [Math.PI * 1.5, 80],
        [Math.PI * 1.75, 80],
        [Math.PI * 2, 80]
    ];
    const radPath = radialLineGen(radPts);
    console.log(radPath);

    //     .attr("class", "pieGen")
    //     .selectAll('path')
    //     .data(arcData)
    //     .enter()
    //     .append("path")
    //     .attr('d', pieGenerator)
    //     // console.log(arcData)
}


/**
 * With the name import must use the curly Braces
 */