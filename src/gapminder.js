import * as d3 from 'd3';

const mainContainer = d3.select("#root");

function createOrderedList(data) {
    const ol = d3.create('ol').node();
    mainContainer.node().append(ol); // Not Declarative

    const n = 10
    const year = 2005
    const listData = data.filter(d => d.year === year).sort((a, b) => b.pop - a.pop).slice(0, n)

    const res = d3.select("ol")
        .selectAll("li")
        .data(data)
        .join(
            enter => enter.append('li'),
            update => update,
            exit => exit.remove()
        )
        .text(d => `${d.country}: ${d.pop}`)
        // return ol.node()
}

function createBarChart(data) {
    const container = d3.create('div').attr("class", "bar-chart");
    mainContainer.node().append(container.node())
    const width = 940;
    // Nest Very Important. Its like group by in sql, key is the element that we
    // must group by, the rollup allows us to do whatever we want with all of the entries
    const barData = d3.nest()
        .key(d => d.cluster)
        .rollup(vals => ~~d3.mean(vals, d => d.life_expect))
        .entries(data.filter(d => d.year === 2005))
        .sort((a, b) => +a.key - +b.key)

    /** Scales are the workhorse of viz design and help code remain as 
     * declarative as possible. For instance we might want to assignt each bar a dofferent
     * color. One option to doing so would info a large ternary expression
     * or if statement. But instead we could use chromatic scale.
     */
    const x = d3.scaleLinear()
        .domain([0, d3.max(barData, d => d.value)])
        .range([0, width]);

    const color = d3.scaleOrdinal(d3.schemeCategory10).domain(barData.map(d => d.key));


    container.selectAll("div")
        .data(barData)
        .join("div")
        .style("background", d => color(d.key))
        .style('border', '1px solid white')
        .style('font-size', "small")
        .style('color', 'white')
        .style('text-align', 'right')
        .style('padding', '3px')
        .style('width', d => `${x(d.value)}px`) // Notice we use the "x" scale instead of a magic number here.
        .text(d => d.value)

    /**
     * At this point, we are rapidly hitting a wall on what we can do with D3 and
     * HTML DOM. HTML is restricted to rectangular shapes and is governed by a
     * complex layout model that can often intefere the way we wisth to posistion and 
     * size marks for viz process. 
     * 
     * As a result rather than perseveromg with HTML, we will instead switch to SVG.
     * Scalable Vector Graphics which gibes us much richer set of graphical 
     * primitives to work with especially with curves, line and arbitraty shapes.
     * These element can be styles using CSS and can be programmatically manipulated via 
     * DOM. S
     */
}

function svgBarChart(data) {
    const width = 940;
    let barHeight = 25,
        height = data.length * barHeight;

    const container = d3.create("svg")
        .attr("width", width)
        .attr("height", height);

    mainContainer.node().append(container.node())

    const barData = d3.nest()
        .key(d => d.cluster)
        .rollup(vals => ~~d3.mean(vals, d => d.life_expect))
        .entries(data.filter(d => d.year === 2005))
        .sort((a, b) => +a.key - +b.key)

    // Scales : To Rotate Chart, Just Redefine scales
    height = 150;
    let x = d3.scaleLinear()
        .domain([0, d3.max(barData, d => d.value)])
        .range([0, width])

    const color = d3.scaleOrdinal(d3.schemeCategory10).domain(barData.map(d => d.key));

    // Flip It
    let y = d3.scaleBand().domain(barData.map(d => d.key)).range([0, height])
    y = d3.scaleBand()
        .domain(barData.map(d => d.key))
        .range([height, 0])


    // Create All THe rectange
    const g = container.selectAll("g")
        .data(barData)
        .join("g")
        .attr("class", "rectangle-group")
        // .attr('transform', d => `translate(0, ${y(d.key)})`);

    // g.append("rect")
    //     .attr("class", "rect-ify")
    //     .attr('x', (d, i) => i * 50)
    //     .attr('y', (d, i) => 30)
    //     .attr("width", 30)
    //     .attr("height", d => newY(d.value)) // Band Scales divide a pizel range into equally size bands
    //     .style("fill", d => color(d.key))
    //     .style("stroke", "white")


    // Now Adding text for same selection

    // g.append("text")
    //     .attr("x", (d, i) => (i * 50))
    //     .attr("y", (d, i) => y(d.key))
    //     .attr('fill', 'blue')
    //     .attr('dx', 10.5)
    //     .attr('dy', '5.2em')
    //     .style('font-size', 'large')
    //     .text(d => d.value)



    /**
     * The Biggest change from shift from HTML and SVG is that we are now entirely
     * responsible for how our bars are laid out. Whereas before we could rely on
     * <div> elements gaining a height based on the content and padding and on the 
     * flow of the layout model to position them one after another. In SVG, we have
     * to explicitly set up x, y positions for all element and the width and height
     * of our <rect>s
     */
}

d3.json('../data/gapminder.json')
    .then(data => {
        svgBarChart(data);
        // createBarChart(data);
    })

// d3.json('../data/barData.json')
//     .then(data => {
//         createBarChart(data);
//     })

/**
 * D3 joins operator gives us a general way to draw viz marks no need to
 * worry about iteration or branchiching. we can focus on what we want the viz
 * to look like rather than writing out each step and how it should
 * be computed. This is called declarative. Data Join make it more 
 * convenient to specify animated transition as items that are 
 * entering, updating or exiting
 */