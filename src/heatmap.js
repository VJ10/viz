import * as d3 from 'd3';

// set the dimensions and margins of the graph
const margin = { top: 80, right: 25, bottom: 30, left: 40 },
    width = 450 - margin.left - margin.right,
    height = 450 - margin.top - margin.bottom;


const svg = d3.selectAll("#viz2");


svg.append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("heigh", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", `translate(${margin.left}, ${margin.top})`);


// Get The Data
async function getData() {
    let heatData = d3.csv("https://raw.githubusercontent.com/holtzy/D3-graph-gallery/master/DATA/heatmap_data.csv");

    const HeatMapDataSet = await heatData;
    const myGroups = HeatMapDataSet.map(row => row.group);
    const myVars = HeatMapDataSet.map(row => row.variable);

    // Build Color Scale
    const myColor = d3.scaleSequential()
        .interpolator(d3.interpolateInferno)
        .domain([1, 100]);

    // Y Axis
    const y = d3.scaleBand()
        .range([height, 0])
        .domain(myVars)
        .padding(0.05);

    svg.append("g")
        .attr("class", "yAxis")
        .style("font-size", 15)
        .call(d3.axisLeft(y).tickSize(0))
        .select(".domain").remove()

    // ToolTip
    const tooltip = d3.select("#viz2")
        .append("div")
        .style("opacity", 0)
        .attr("class", "tooltip")
        .style("background-color", "white")
        .style("border", "solid")
        .style("border-width", "2px")
        .style("border-radius", "5px")
        .style("padding", "5px");

    // Three function that change the tooltip when user hover / move / leave a cell
    const mouseover = function(d) {
        tooltip
            .style("opacity", 1)
        d3.select(this)
            .style("stroke", "black")
            .style("opacity", 1)
    }
    const mousemove = function(d) {
        tooltip
            .html("The exact value of<br>this cell is: " + d.value)
            .style("left", (d3.mouse(this)[0] + 70) + "px")
            .style("top", (d3.mouse(this)[1]) + "px")
    }
    const mouseleave = function(d) {
        tooltip
            .style("opacity", 0)
        d3.select(this)
            .style("stroke", "none")
            .style("opacity", 0.8)
    }

    // add the squares
    //     svg.selectAll()
    //         .data(data, function(d) { return d.group + ':' + d.variable; })
    //         .enter()
    //         .append("rect")
    //         .attr("x", function(d) { return x(d.group) })
    //         .attr("y", function(d) { return y(d.variable) })
    //         .attr("rx", 4)
    //         .attr("ry", 4)
    //         .attr("width", x.bandwidth())
    //         .attr("height", y.bandwidth())
    //         .style("fill", function(d) { return myColor(d.value) })
    //         .style("stroke-width", 4)
    //         .style("stroke", "none")
    //         .style("opacity", 0.8)
    //         .on("mouseover", mouseover)
    //         .on("mousemove", mousemove)
    //         .on("mouseleave", mouseleave)
    // )





}

getData();