import * as d3 from 'd3'
import { treemapSlice } from 'd3'
import colorbrewer from "colorbrewer"


// Chapter 3
const width = 500;
const height = 500;
const container = d3.select('#root');

function createViz() {
    d3.csv('../data/sample2.csv').then(data => overallViz(data))
}

function overallViz(incomingData) {

    container.append('svg').attr('style', `width:${width}px;height:${height}px;border:1px lightgray solid;`);
    container.append('div').attr('id', 'controls')

    d3.select("svg")
        .append("g")
        .attr("id", "teamsG")
        .attr("transform", "translate(50,300)")
        .selectAll("g")
        .data(incomingData)
        .enter()
        .append("g")
        .attr("class", "overrallG")
        .attr("transform", (d, i) => `translate(${(i * 50)}, 0)`)

    let teamG = d3.selectAll("g.overrallG") //Get all of the group that we just created
    teamG.append("circle")
        .attr("r", 0)
        .transition()
        .delay((d, i) => i * 100)
        .duration(500)
        .attr("r", 20)

    teamG
        .append("text")
        .style("text-anchor", "middle") // used to center text in SVG
        .attr("y", 30)
        .text(d => d.team)

    const dataKeys = Object.keys(incomingData[0]).filter(d => d !== "team" && d !== "region");

    d3.select("#controls")
        .selectAll("button.teams")
        .data(dataKeys)
        .enter()
        .append("button")
        .on("click", buttonClick) // Register onClick behavior for each button along with a wrapper that gives access to the data that it was bound when it was created.
        .html(d => d) //Could hardcode buttons in another html part but we wantmore scalable buttons.

    // Side Bar
    d3.text('./Data/infobox.html')
        .then(htmlData => {
            container.append("div")
                .attr("id", "infobox")
                .html(htmlData)
        })

    function buttonClick(datapoint) {
        const maxValue = d3.max(incomingData, d => parseFloat(d[datapoint]));
        // alert(`${datapoint} : ${maxValue}`);
        const radiusScale = d3.scaleLinear()
            .domain([0, maxValue])
            .range([2, 20])
            .unknown("#c4b9ac");

        /**
         *  D3 also spupport two other color interpolators, HCL (Hue, Chroma, Lightness) and LAB (Lightness A-b) which each deal
         *  in diffenent manner with question of what color are between blue and yellow.
         * In general color interpolated in RGB tends to be muddy and gray. We can either rely on
         * d3 exports or make our our like ybRamp.
         */
        const ybRamp = d3.scaleLinear()
            .interpolate(d3.interpolateLab) // great if we dont want to use mixing which is the default scale when creating color scale
            .domain([0, maxValue])
            .range(["blue", "yellow"]);

        /**
         * Usually better to use discrete color scales availaible.a1
         *  schemeCategory10, schemeCategory20,schemeCategory20,schemeCategory20,
         * these are arrays of color mean to be padded to scaleOrdinal which can be used to
         * map categorical values to particular colors
         */
        const tenColorScale = d3.scaleOrdinal()
            .domain(["UEFA", "CONMEBOL", "CONCACAF", "AFC"])
            .range(d3.schemeCategory10)
            .unknown("#c4b9ac");

        console.log("Experiment")
        console.log(d3.schemeCategory10)
        console.log(colorbrewer.Reds)
        console.log(this); // the object that was use with on. In this case its button. tie events to any object
        console.log("Over")

        const colorQuantize = d3.scaleQuantize()
            .domain([0, maxValue])
            .range(colorbrewer.Reds[3]);

        /**
         * The Instre gives yoy the capacity to tell D3 to insert the images
         * before the text elements. This keeps the label from being drawn behind
         * newly added images
         */
        d3.selectAll("g.overrallG")
            // .insert("image", "text")  // more specific tell, add image before text
            // .attr("xlink:href", d => `images/${d.team}.png`)
            // .attr("width", "45px").attr("height", "20px")
            // .attr("width", "45px").attr("height", "20px")
            .select("circle")
            .transition()
            .delay((d, i) => i * 100)
            .duration(1000)
            // .style("fill", d => ybRamp(d[datapoint]))
            // .style("fill", d => tenColorScale(d.region))
            .style("fill", d => colorQuantize(d[datapoint]))
            .attr("r", d => radiusScale(d[datapoint]));

        // d3.text(htmlTable)
        //     .then(tableData => console.log(tableData))
    }

    teamG.on("mouseover", highlightRegion)
    teamG.on("mouseout", unHighlight)
    teamG.on("click", teamClick)

    function highlightRegion(d, i) {
        const teamColor = d3.rgb("#75739F");
        // console.log(" Begin Inside")
        // console.log(d);
        // console.log(i)
        // console.log(this) // actual element
        // console.log("END Inside")

        d3.select(this)
            .select("text")
            .classed("active", true)
            .attr("y", 10);

        d3.selectAll("g.overrallG")
            .select("circle")
            .style("fill", p => p.region === d.region ? teamColor.darker(.75) : teamColor.brighter(0.5))
            // .each(function(p) {
            //     p.region == d.region ? d3.select(this).classed("active", true) : d3.select(this).classed("inactive", true)
            // })

        // this.parentElement.append(this); // moves elemt to the end of the at dom region so that its drawm above everything
        // Raise & Lower
        d3.select(this).raise()
        teamG.select("text").style("pointer-events", "none") // disable muse event on elements


    }

    function teamClick(d) {
        d3.selectAll("td.data").data(d3.values(d))
            .html(p => p);
        // console.log("Inside Team")
        // console.log(d)
        // console.log(this)
        // console.log("Done Team")
    }

    function unHighlight() {
        d3.selectAll("g.overrallG").select("circle").attr("class", "")
        d3.selectAll("g.overrallG").select("text").classed("active", false).attr("y", 30)
    }



}

createViz();

/**
 * Inline function always have access to the DOM element along with bound data. Arrow Function does not.
 * But there is a way to actually get access to the actual DOM element, can use 3 params (data, index, nodes)
 * d3.select("circle").each((d, i, nodes) => console.log(nodes[i]))
 * 
 * D3 also have many helper fo th ecolor such as rgb ehicch access color name "red", hex value, rgb(255,0,0) values.
 * its straightforward and easy to load externally generated resources like images,HTML fragment,
 * and pregenerated SVG and tie them to your graphical elemnts.
 * 
 * Summary
 * D3 includes useful functionality beyond formating data to create charts such as 
 * text() and html() for loading text and html as data.a1
 * Using color righ is challenging and color is underutilized but rules exist for 
 * using color as well
 * Triggerring animated transition with mouse event using selection.on() and 
 * transition() smart way to improve user experience
 * External SVG icons can e loaded and then styled or color based on data for
 * more infographics style charting.
 */