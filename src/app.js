import * as d3 from 'd3'

const mainContainer = d3.select("#root");
const svgContainer = mainContainer
    .append("svg")
    .attr("width", 600)
    .attr('height', 600);

const scatterData = [
    { friends: 5, salary: 22000 },
    { friends: 3, salary: 18000 },
    { friends: 10, salary: 88000 },
    { friends: 0, salary: 180000 },
    { friends: 27, salary: 56000 },
    { friends: 8, salary: 74000 }
];

function scatterPlot() {
    const xExtent = d3.extent(scatterData, d => d.salary);
    const yExtent = d3.extent(scatterData, d => d.friends);

    const xScale = d3.scaleLinear().domain(xExtent).range([0, 500]);
    const yScale = d3.scaleLinear().domain([0, 27]).range([0, 500]);
    svgContainer.attr("transform", `translate(0,50)`)

    /**
     * =============
     * Creating Axes
     * =============
     * D3 provides d3.axisLeft(), d3.axisRight(), d3.axisBotton() and d3.axisTop()
     * selection's cakk() method from a selection on g element we want these graphical
     * ekements to be drawn.
     * 
     * it a component that is commpose of a <g.tick> that contains a line. <path.domain> with 
     * equal size to the extent of the axis and finally a label which contans a text
     * for each tick. axisLeft() or axisTop(), are either drawn outside the canvas like
     * earlier rectangle. to move the axes aroung then we must use .attr("translate") of
     * their parent <g> elements, either when we draw them or later. This is why its important
     * to assign ID to our element when we append them to hte canvas. We can move the 
     * x-axis to the bottom.
     *  */
    const yAxis = d3.axisRight().scale(yScale).ticks(16) //.tickSize(505);
    const xAxis = d3.axisBottom().scale(xScale).ticks(10) //.tickSize(510);

    // Adding Axes To SVG.
    svgContainer.append("g")
        .attr("id", "yAxisG")
        .call(yAxis);

    svgContainer.append("g")
        .attr("id", "xAxisG")
        .attr("transform", "translate(0,0)")
        .call(xAxis); // thats where we want these little graphical elements to be drawn

    svgContainer.selectAll("circle")
        .data(scatterData)
        .enter()
        .append("circle")
        .attr("r", 5)
        .attr("cx", (d, i) => xScale(d.salary)) // higher salary mean more on the right
        .attr("cy", d => d.friends) // circle place in the lower in the charts represent, individual with more friends
}

function creatingBoxPlot(data) {
    const xScale = d3.scaleLinear().domain([1, 8]).range([20, 470]); //d3.scaleLinear().domain([1, 8]).range([20, tickSize]);
    const yScale = d3.scaleLinear().domain([0, 100]).range([480, 20]); //d3.scaleLinear().domain([0, 100]).range([tickSize + 10, 20]);

    // Y Scale
    const yAxis = d3.axisRight()
        .scale(yScale)
        .ticks(8)
        .tickSize(-470);

    d3.select("svg").append("g")
        .attr("transform", "translate(470,0)")
        .attr("id", "yAxisG")
        .call(yAxis);

    // X Scale
    const xAxis = d3.axisBottom()
        .scale(xScale)
        .tickSize(-tickSize)
        .tickValues([1, 2, 3, 4, 5, 6, 7])

    d3.select("svg").append("g")
        .attr("transform", "translate(0,480)")
        .attr("id", "xAxisG")
        .call(xAxis);


    // d3.select("svg").append("g").attr("transform", `translate(${tickSize},0)`).attr("id", "yAxisG").call(yAxis)
    // d3.select("svg").append("g").attr("transform", `translate(0,${tickSize + 10})`).attr("id", "xAxisG").call(xAxis)

    /**
     * We can use .each() function of a selection which allows us to perfor the same code on each element to create new elements.
     */
    // Circle Time
    d3.select("svg")
        .selectAll("circle.median")
        .data(data)
        .enter()
        .append("circle")
        .attr("class", "median")
        .attr("r", 5)
        .attr("cx", d => xScale(d.day))
        .attr("cy", d => yScale(d.median))
        .style("fill", "red");

    // Initial BoxPlot. We need ti add a <g> element for charts since it allows us to apply label or other important information. 
    // .each() allows us to access the bound data, array posistion and the DOM element
    d3.select("svg")
        .selectAll("g.box")
        .data(data)
        .enter()
        .append("g")
        .attr("class", "box")
        .attr("transform", d => "translate(" + xScale(d.day) + "," + yScale(d.median) + ")")
        .each(function(d, i) {
            d3.select(this)
                .append("rect")
                .attr("width", 20)
                .attr("x", -10) // so can be centered rectangle horizontally
                .attr("y", yScale(d.q3) - yScale(d.median))
                .attr("height", yScale(d.q1) - yScale(d.q3))
                .style("fill", "white")
                .style("stroke", "black");
        })
}

// Run Function
const tickSize = 470;
scatterPlot();
// d3.csv("../data/boxplot.csv").then(data => creatingBoxPlot(data));

/**
 * ======================================================================================
 * Chart Components
 * ======================================================================================
 * D3 also includes layout which allows the creation of complex data viz from a properly
 * formatted data set. D3 Component like an axis, is a function for drawing all the
 * graphical elements necessary for an axis. A Generator like d3.line(), lets you draw
 * straight or curved line across many points.
 * 
 * Charting Principles
 * ===================
 * All Charts consits of several graphical element that are drawn or derived from the
 * dataset being represented. These elements may be graphical primitives such as circles
 * or rectangles, or more complex, multipart, graphical objects like the boxplots. its
 * important to differentiate between the methods availaible in D3 to create graphics for
 * charts. There are 3 main types of function found in D3: Generators, Components 
 * and layout.
 * 
 * Generators
 * ==========
 * Consists of function that take data and return the necessary SVG drawing code to create
 * graphical object based on that data. For instance, if you have an array of poing and 
 * you want to draw line from one point to another or turn it into a polygon or an area.
 * A few d3 function can help you with this process. These generators simplify the process
 * of creating complex svg <path> by abstracting the process need to write a path's d 
 * attribute. We look at three generator in this chapter line, area, diagonal and arc.
 * 
 * Components
 * ==========
 * Creates an entire set of graphical objects necessary for a particular chart component. 
 * Most popular chart component is d3.axis which create bunch of line, path, g and text
 * elements that are needed for an axis based on the scale and setting that we provide
 * the function.
 * 
 * Layouts
 * =======
 * More straigtforward, like the pie chart layour or complex like force directed network
 * layouts. It take in one or more arrays of data and sometimes generator and append
 * attributes to the data necessary to draw it in certain position or sizes, either 
 * statically or dynamically to the data necesary to draw it in certain positions or sizes. 
 * 
 */