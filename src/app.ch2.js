import * as d3 from 'd3'
import { learningAboutGenerator } from "./generator.js";

const width = 600;
const height = 480;
const margin = { top: 0, bottom: 0, left: 0, right: 0 }

// Rectangle Ex.
const drawRectangles = () => {
    const rectData = [15, 50, 22, 8, 100, 10];
    d3.select('#root')
        .append("svg")
        .selectAll("rect")
        .data(rectData)
        .enter()
        .append("rect")
        .attr("width", 10)
        .attr("height", d => d)
        .attr("x", (d, i) => i * 10)
        .attr("y", (d) => 100 - d)
        .style("opacity", .25)
        .style("stroke", "#9A8B7A")
        .style("stroke-width", "1px");
}

// 2.3.1 Visualization From Loaded Data
d3.csv('data/cities.csv')
    .then((data) => {
        //--dataViz(data);
    })
    .catch((error) => {
        console.error(error);
    })

const dataViz = (incomingData) => {
    const maxPopulation = d3.max(incomingData, d => parseInt(d.population));

    let yScale = d3.scaleLinear()
        .domain([0, maxPopulation])
        .range([0, 460]);

    d3.select("#root")
        .append("svg")
        .attr("style", `height:${height}px; width:${width}px`);

    d3.select("svg")
        .selectAll("rect")
        .data(incomingData)
        .enter()
        .append("rect")
        .attr('class', 'cities')
        .attr("width", 50)
        .attr("height", d => yScale(parseInt(d.population)))
        .attr("x", (d, i) => i * 60)
        .attr("y", d => height - yScale(parseInt(d.population)))
        .style("fill", "#FE9922")
        .style("stroke", "#9A8B7A")
        .style("stroke-width", "1px");

}


// Nested Data
d3.json("data/data.json")
    .then(d => {
        // nestedPosts(d.tweets);
        ScatterPosts(d.tweets);
    })
    .catch(err => console.log(err));

const nestedPosts = (incomingData) => {
    let nestedTwts = d3.nest()
        .key(d => d.user)
        .entries(incomingData);

    nestedTwts.forEach(d => {
        d.numPosts = d.values.length;
    })


    const maxTweets = d3.max(nestedTwts, d => d.numPosts);
    const yScale = d3.scaleLinear().domain([0, maxTweets]).range([0, 500]);

    d3.select("#root")
        .append("svg")
        .attr("style", "height: 480px; width: 600px;")
        .selectAll("rect")
        .data(nestedTwts)
        .enter()
        .append("rect")
        .attr("width", 50)
        .attr("height", d => yScale(d.numPosts))
        .attr("x", (d, i) => i * 60)
        .attr("y", d => 480 - yScale(d.numPosts))
        .style("fill", "#FE9922")
        .style("stroke", "#9A8B7A")
        .style("stroke-width", "1px");

}

const ScatterPosts = (data) => {
    data.map(d => {
        d.impact = d.favorites.length + d.retweets.length
        d.tweetTime = new Date(d.timestamp)
    });

    const maxImpact = d3.max(data, d => d.impact);
    const startEnd = d3.extent(data, d => d.tweetTime);
    const timeRamp = d3.scaleTime().domain(startEnd).range([20, 480]);
    const yScale = d3.scaleLinear().domain([0, maxImpact]).range([0, 460]);
    const radiusScale = d3.scaleLinear().domain([0, maxImpact]).range([1, 20]);
    const colorScale = d3.scaleLinear().domain([0, maxImpact]).range(["white", "#75739F"]);

    // Generators
    // learningAboutGenerator();

    let tweeG = d3.select("#root")
        .append("svg")
        .attr("style", "height: 480px; width: 600px;")
        .selectAll("circle")
        .data(data)
        .enter()
        .append("g")
        .attr("transform", d => `translate(${timeRamp(d.tweetTime)},${480 - yScale(d.impact)})`);

    tweeG
        .append("circle")
        .attr("r", d => radiusScale(d.impact))
        .style("fill", "#75739F")
        .style("stroke", "black")
        .style("stroke-width", "1px");

    tweeG.append("text")
        .text(d => d.user + "-" + d.tweetTime.getHours());
    // .attr("cx", d => timeRamp(d.tweetTime))
    // .attr("cy", d => 480 - yScale(d.impact))
    // .style("fill", d => colorScale(d.impact))

    let filteredData = data.filter(d => d.impact > 0)
    d3.selectAll("circle")
        .data(filteredData, d => JSON.stringify(d))
        .exit()
        .remove();



    // genFunctions();
    // // Generator : Takes data and then computes the necessary angle to represent the data like Pie, Line etc ...
    // const fruits = [{ 'name': "Apples", 'quantity': 20 },
    //     { 'name': "Bananas", 'quantity': 40 },
    //     { 'name': "Cherries", 'quantity': 50 },
    //     { 'name': "Damsons", 'quantity': 10 },
    //     { 'name': "Elderberries", 'quantity': 30 }
    // ];

    // var pieGenerator = d3.pie().value(d => d.quantity).sort((a, b) => a.name.localeCompare(b.name));
    // const arcData = pieGenerator(fruits);

    // console.log(arcData)

    // d3.select("#root")
    //     .select("svg")
    //     .append("g")
    //     .attr("class", "pieGen")
    //     .selectAll('path')
    //     .data(arcData)
    //     .enter()
    //     .append("path")
    //     .attr('d', pieGenerator)
    //     // console.log(arcData)


};


//   // Loading external data
//   const dataset = await d3.csv('/data/sample.csv')
//   dataset.forEach((data) => {
//     console.log(data)
//   })
// }) ()